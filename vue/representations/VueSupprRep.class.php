<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Page de suppression d'une representation
 * @author Antoine T
 * @version 2020
 */
class VueSupprRep extends VueGenerique {


    private $uneRepresentation;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer la representation ?
            <h3><br>
                <a href="index.php?controleur=groupes&action=validerSupprimer&id=<?= $this->uneRepresentation->getId() ?>">Oui</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="index.php?controleur=representations">Non</a></h3>
        </center>
        <?php
        include $this->getPied();
    }

    function setUneRepresentaions(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }
}
