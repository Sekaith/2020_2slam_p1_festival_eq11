<?php
/**
 * Description Page de consultation des offres d'hébergement par établissmeent
 * -> affiche une page comportant un tableau par établissement, indiquant 
 * pour chaque type de chambre, le nombre de chambres offertes pour cet établissment
 * @author prof
 * @version 2018
 */

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\DaoRepresentation;
use modele\metier\Lieu;
use modele\metier\Groupe;

class VueListeRepresentations extends VueGenerique {
private $lesRepresentations;

public function __construct() {
    parent::__construct();
}

public function afficher() {
    include $this->getEntete();
    
    
    
    
    if (count($this->lesRepresentations) != 0) {
        $lesDates = DaoRepresentation::getAllDate();
        if(is_array($lesDates)){
            foreach ($lesDates as $uneDate){ ?>
                <strong><?= $uneDate?></strong><br>
                
                <table width="45%" cellspacing="0" cellpadding class="tabQuadrille">
                    <tr class="enTeteTabQuad">
                        <td width="25%">Groupe</td>
                        <td width="25%">Lieu</td>
                        <td width="25%">Heure Debut</td>
                        <td width="25%">Heure Fin</td>
                        <td width="35%"></td>
                        <td width="35%"></td>
                    </tr>
                    <?php 
                    foreach ($this->lesRepresentations as $uneRepresentation){?>
                    <tr class="ligneTabQuad">
                        <?php if(($uneRepresentation->getDateRep()) == $uneDate) {?>
                        <td><?= $uneRepresentation->getLeGroupe()->getNom() ?></td>
                        <td><?= $uneRepresentation->getLeLieu()->getNomLieu() ?></td>
                        <td><?= $uneRepresentation->getHeureDebut() ?></td>
                        <td><?= $uneRepresentation->getHeureFin() ?></td>
                        <td> 
                            <a href="index.php?controleur=representations&action=modifier&id=<?= $id ?>" >
                                Modifier
                            </a>
                        </td>
                        <td> 
                            <a href="index.php?controleur=representations&action=supprimer&id=<?= $id ?>" >
                               Supprimer
                            </a>
                        </td> 
                    </tr>
                    <?php

                        }
                    }?>
                </table>
                <br>
                <?php
            }
        }
        ?>
        <a href="index.php?controleur=representations&action=creer" >
           Création d'une representation</a >
        <?php
    }
    include $this->getPied();
    
}
public function setLesRepresentations(array $lesRepresentations){
    $this->lesRepresentations = $lesRepresentations;
}
}
                

