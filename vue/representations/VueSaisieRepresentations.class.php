<?php

namespace vue\groupes;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description Page de saisie/modification d'une representation donnée
 * @author prof
 * @version 2018
 */
class VueSaisieGroupes extends VueGenerique {

    /** @var Representation à afficher */
    private $uneRepresentation;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message; 

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <tr class="ligneTabNonQuad">
                    <td> Groupe*: </td>
                    <td><input type="text" pattern="[A-Za-zéèêàçë -]{1,}" value="<?= $this->uneRepresentation->getLeGroupe() ?>" name="groupe" size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Lieu*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getLeLieu() ?>" name="lieu" 
                               size="50" maxlength="45"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure Debut*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureDebut() ?>" name="heuredebut" 
                               size="7" maxlength="5"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure Fin*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getHeureFin() ?>" name="heurefin" size="40" 
                               maxlength="35"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="text" value="<?= $this->uneRepresentation->getDateRep() ?>" name="date" size ="20" 
                               maxlength="10"></td>
                             
                </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representations&action=liste">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }

    public function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }


    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

}

