/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Mathias
 * Created: 6 oct. 2020
 */

ALTER TABLE `Representation` ADD CONSTRAINT `fk_lieu` FOREIGN KEY (`id_lieu`) REFERENCES `Lieux`(`id_lieu`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `Representation` ADD CONSTRAINT `fk_grp` FOREIGN KEY (`id_groupe`) REFERENCES `Groupe`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;