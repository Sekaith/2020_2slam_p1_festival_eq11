/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Mathias
 * Created: 6 oct. 2020
 */

insert into Lieux (id_lieu, nom_lieu, adresse_lieu, capacite) values (01,'Le cabaret','221b Baker Street',200);
insert into Lieux (id_lieu, nom_lieu, adresse_lieu, capacite) values (02,'Le village','19 quai de Bourbon',500);
insert into Lieux (id_lieu, nom_lieu, adresse_lieu, capacite) values (03,'Salle des fete de Saint pierre en Faucigny ','109 rue de Richelieu',350);
insert into Lieux (id_lieu, nom_lieu, adresse_lieu, capacite) values (04,'Le Parc des Ronces','2 rue Charles Dubois',250);
insert into Lieux (id_lieu, nom_lieu, adresse_lieu, capacite) values (05,'Stade des petits poulets','Down House',750);